const Koa = require('koa')
const app = new Koa()
const koaRouter = require('koa-router')()
const bodyParser = require('koa-bodyparser')

const authen = require('./middlewares/authen')
const validator = require('./middlewares/validator')

module.exports = {
    start
}

function start() {
    const port = 5000

    koaRouter.get('/',

        async (ctx, next) => {
            ctx.status = 200
            ctx.body = {
                name: `node-training2`
            }
        }

    )

    // get hotel by id
    koaRouter.get('/api/v1/hotel/:id/',

        async (ctx, next) => {
            ctx.status = 200
            ctx.body = {
                name: 'Hotel1'
            }
        }
    )

    // search hotels
    koaRouter.post('/api/v1/hotels/',

        // middleware: authen
        authen.authenticate,

        // middleware: validate
        validator.validate,

        async (ctx, next) => {
            console.log('controller enter')

            ctx.status = 200
            ctx.body = [
                'Hotel1',
                'Hotel2',
                'Hotel3',
                'Hotel4',
            ]
        }
    )

    app.use(bodyParser())
    app.use(koaRouter.routes())
    app.use(koaRouter.allowedMethods())
    app.listen(port)
    console.log(`server is running at port ${port}`)
}