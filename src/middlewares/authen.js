module.exports = {
    authenticate
}

async function authenticate(ctx, next) {
    console.log('authen enter')

    const token = ctx.get('token')
    console.log(`token: ${token}`)

    if (token != '1234') {
        ctx.status = 401
        return ctx.body = {
            result: 'Unauthorized'
        }
    }

    return next()
}