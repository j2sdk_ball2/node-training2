module.exports = {
    validate
}

async function validate(ctx, next) {
    console.log('validate enter')
    const { id } = ctx.request.body

    console.log(`id: ${JSON.stringify(id)}`)

    if (!id) {
        ctx.status = 400
        return ctx.body = {
            result: 'Bad Request'
        }
    }

    return next()
}